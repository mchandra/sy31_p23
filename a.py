
import rospy

import numpy as np

import cv2

from cv_bridge import CvBridge, CvBridgeError

from sensor_msgs.msg import Image



class CameraNode:

    def __init__(self):

        # Creates a node called and registers it to the ROS master
        rospy.init_node('detect')

        # CvBridge is used to convert ROS messages to matrices manipulable by OpenCV
        self.bridge = CvBridge()

        # Initialize the node parameters
        # Publisher to the output topics.
        self.pub_img = rospy.Publisher('~output', Image, queue_size=10)

        # Subscriber to the input topic. self.callback is called when a message is received
        self.subscriber = rospy.Subscriber('/camera/image_color', Image, self.callback)

        
    def detect_shape(self, cnt):

        #approximate the contour
        peri = cv2.arcLength(cnt, True)
        approx = cv2.approxPolyDP(cnt, 0.02 * peri, True)
        shape = "unidentified"

        # putting shape name at center of each shape
        if len(approx) == 3:
            shape = "rectangle"
        elif len(approx) == 4:
            (x, y, w, h) = cv2.boundingRect(approx)
            ar = w / float(h)
            #a square will have an aspect ratio that is approximately equal to one, otherwise, the shape is a rectangle
            shape = "rectangle"
        elif len(approx) == 5:
            shape = "rectangle"
        elif len(approx) == 6:
            shape = "rectangle"
        else:
            shape = "cercle"
        return shape

    def draw_max_area(self, img_bgr, max_area, max_contour):
        #obtain the bounding rectangle coordinates using cv2.boundingRect() then extract the ROI using Numpy slicing.
        x, y, w, h = cv2.boundingRect(max_contour)
        roi = img_bgr[y:y+h, x:x+w]
        #draw the bounding box on the original image using cv2.rectangle()
        cv2.rectangle(img_bgr, (x, y), (x+w, y+h), (0, 255, 0), 2)
        #Déterminer une forme englobante (hull, rectangles ou cercles)
        hull = cv2.convexHull(max_contour)
        cv2.drawContours(img_bgr, [hull], -1, (0, 255, 0), 2)
        shape = self.detect_shape(max_contour)
        #print(shape)
        
        # Draw the center of the contour
        M = cv2.moments(max_contour)
        if M["m00"] != 0:
            cX = int(M["m10"] / M["m00"])
            cY = int(M["m01"] / M["m00"])
            
            cv2.circle(img_bgr, (cX, cY), 7, (255, 255, 255), -1)
            cv2.putText(img_bgr, "center", (cX - 20, cY - 20),
                cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
        
        # Draw the contour area
        cv2.putText(img_bgr, "area: {:.2f}".format(max_area), (10, 30),
            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
        
        # Draw the shape
        shape = self.detect_shape(max_contour)
        cv2.putText(img_bgr, "shape: {}".format(shape), (10, 50),
            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
        
        return img_bgr


    def callback(self, msg):

        '''
        Function called when an image is received.
        msg: Image message received
        img_bgr: Width*Height*3 Numpy matrix storing the image
        '''

        # Convert ROS Image -> OpenCV
        try:
            img_bgr = self.bridge.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError as e:
            rospy.logwarn(e)
            return
        # TODO
        
        def max_area(contours):        
        # Find the largest area contour
            if contours != []:
                max_area = -1
                for i in range(len(contours)):
                    area = cv2.contourArea(contours[i])
                    if area > max_area:
                        max_area = area 
                return max_area
        
        def calculate_shape_size(contour):
            # Compute the bounding box of the contour
            x, y, w, h = cv2.boundingRect(contour)
            
            # Calculate the area of the contour
            area = cv2.contourArea(contour)
            
            # Calculate other size-related measurements if needed
            # For example, you can calculate the width, height, or aspect ratio
            
            width = w
            height = h
            aspect_ratio = float(w) / h
            
            return area, width, height, aspect_ratio


    
        
        #plage des couleur en hsv
        lower_red = np.array([335/2, 20*2.55, 15*2.55])
        upper_red = np.array([360/2, 100*2.55, 90*2.55])
        red_hsv = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2HSV)

        lower_blue = np.array([190/2, 20*2.55, 50*2.55])
        upper_blue = np.array([225/2, 100*2.55, 90*2.55])
        blue_hsv = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2HSV)
   
        lower_white = (100,5 ,45)
        upper_white = (250, 64, 95)
        white_hsv = cv2.cvtColor(img_bgr, cv2.COLOR_BGR2HSV)
    
        #mask
        mask_red = cv2.inRange(red_hsv, lower_red, upper_red)
        mask_blue = cv2.inRange(blue_hsv, lower_blue, upper_blue)
        mask_white = cv2.inRange(white_hsv, lower_white, upper_white)
            

        #find contours
        contours_red, hierarchy = cv2.findContours(mask_red, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        contours_blue, hierarchy = cv2.findContours(mask_blue, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours_white, hierarchy = cv2.findContours(mask_white, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        max_red_area = max_area(contours_red)
        max_blue_area = max_area(contours_blue)
        max_white_area = max_area(contours_white)
        maxarea = 80000
        
        color = "No color detected"
        
        #objetc size
        size_red = len(contours_red)
        size_blue = len(contours_blue)

        if max_red_area == None :
            max_red_area = -1
        if max_blue_area == None : 
            max_blue_area = -1
        if max_white_area == None :
            max_white_area = -1

        #find the biggest contour
        if max_red_area > max_blue_area:
            c_red = max(contours_red, key = cv2.contourArea)
            area_red = cv2.contourArea(c_red)
            if area_red > maxarea:
                shape = self.detect_shape(c_red)
                cv2.drawContours(img_bgr, [c_red], -1, (0, 255, 0), 3)
                cv2.putText(img_bgr, shape, (c_red[0][0][0], c_red[0][0][1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                #draw the contour and center of the shape on the image
                M = cv2.moments(c_red)
                cx, cy = int(M['m10']/M['m00']), int(M['m01']/M['m00'])
                cv2.circle(img_bgr, (cx, cy), 5, (0, 0, 255), -1)
                #shape = self.detect_shape(c_red)
                img_bgr = self.draw_max_area(img_bgr, area_red,c_red)
                #print(shape)
                color = "rouge"
                
        if max_blue_area > max_red_area:
            c_blue = max(contours_blue, key = cv2.contourArea)
            area_blue = cv2.contourArea(c_blue)
            if area_blue > maxarea:
                shape = self.detect_shape(c_blue)
                cv2.drawContours(img_bgr, [c_blue], -1, (0, 255, 0), 3)
                cv2.putText(img_bgr, shape, (c_blue[0][0][0], c_blue[0][0][1]), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                M = cv2.moments(c_blue)
                cx, cy = int(M['m10']/M['m00']), int(M['m01']/M['m00'])
                cv2.circle(img_bgr, (cx, cy), 5, (0, 0, 255), -1)
                #shape = self.detect_shape(c_blue)
                img_bgr = self.draw_max_area(img_bgr, area_blue,c_blue)
                #print(shape)
                color = "bleu"
                
        print("Un "+ shape + " " + color)

        # Convert OpenCV -> ROS Image and publish
        try:
            self.pub_img.publish(self.bridge.cv2_to_imgmsg(img_bgr, "bgr8")) # /!\ 'mono8' for grayscale images, 'bgr8' for color images
        except CvBridgeError as e:
            rospy.logwarn(e)



if __name__ == '__main__':
    # Start the node and wait until it receives a message or stopped by Ctrl+C
    node = CameraNode()
    rospy.spin()
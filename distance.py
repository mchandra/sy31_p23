#!/usr/bin/env python3
# coding: utf-8

"""
PROJET SY31 P21: HO Xuan Vinh
"""

import numpy as np
import rospy
from geometry_msgs.msg import PoseStamped
from std_msgs.msg import Int32, Float32
from geometry_msgs.msg import Twist, Point32
from sensor_msgs.msg import LaserScan, PointCloud2, PointField
from sensor_msgs.msg import Imu, MagneticField
from tf.transformations import quaternion_from_euler, euler_from_quaternion


PC2FIELDS = [PointField('x', 0, PointField.FLOAT32, 1),
             PointField('y', 4, PointField.FLOAT32, 1),
             PointField('z', 8, PointField.FLOAT32, 1),
             PointField('c', 12, PointField.INT16, 1)
]

class moving:

    def __init__(self):
        rospy.init_node('moving', anonymous=True)
     
        self.subcriber_laser = rospy.Subscriber('/scan', LaserScan, self.function_laser)

        self.d_forward = 0
        self.d_left = 0
        self.d_right = 0
        self.d_back=0


    def function_laser(self,msg):
        coords = []
        obstacles = []
        for i, theta in enumerate(np.arange(msg.angle_min, msg.angle_max, msg.angle_increment)):
            # ToDo: Remove points too close
            if msg.ranges[i] < 0.1:
                continue
            # ToDo: Polar to Cartesian transformation
            coords.append([msg.ranges[i]*np.cos(theta), msg.ranges[i]*np.sin(theta)])
        #print(coords)
        distance = []
        for point in coords:
            distance.append(np.sqrt(point[0]*point[0]+point[1]*point[1]))
        first = min(distance[0:25])
        second = min(distance[175:200])
        self.d_forward =  min(first,second)
        self.d_right = min(distance[26:75])
        self.d_back = min(distance[76:125])
        self.d_left = min(distance[126:175])
        print(min(first,second))

if __name__ == '__main__':
    try:
        moving()
        rospy.spin()
    except rospy.ROSInterruptException:
        pass